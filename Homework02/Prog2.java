import java.util.Scanner;

public class Prog2 {
    public static void print(int x) {
        if (x >= 0 && x <= 127) {
            if (x < 2) {
                System.out.print(x);
                return;
            }
            int y = x / 2;
            print(y);
            int ostatok = x % 2;
            System.out.print(ostatok);
        } else System.out.printf("Не верное число");
    }

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        print(x);
    }
}
