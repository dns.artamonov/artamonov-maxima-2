import java.util.Scanner;

public class Prog {
    public static void main(String[] args) {
        int[] arr = new int[121];
        Scanner scanner = new Scanner(System.in);
        int tmp = scanner.nextInt();
        while (tmp != -1) {
            arr[tmp] += +1;
            tmp = scanner.nextInt();
        }
        int maxAgeCount = 0;
        int indexMax = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > maxAgeCount) {
                maxAgeCount = arr[i];
                indexMax = i;
            }
        }
        System.out.println("Вывод - " + indexMax + " лет");
    }
}
