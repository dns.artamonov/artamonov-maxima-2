
public class Driver {
    private String name;
    private int experience;
    private Bus bus;

    public String getName() {
        return name;
    }

    public Driver(String name, int experience) {
        this.name = name;
        if (experience > 0) {
            this.experience = experience;
        } else {
            System.err.println("ОПЫТА у " + name + "НЕТ!");
        }
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public void drive() throws InterruptedException {
        bus.setDriving(true);
        bus.driving();
    }

    public void stop() {
        bus.setDriving(false);
    }

}
