import org.w3c.dom.ls.LSOutput;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Driver driver = new Driver("СуперВодила", 10);

        Bus bus = new Bus(99, "Сокол тысячетелия", 5);
        Passenger passenger1 = new Passenger("Марсель");
        Passenger passenger2 = new Passenger("Виктор");
        Passenger passenger3 = new Passenger("Чуи");
        Passenger passenger4 = new Passenger("Сергей");
        Passenger passenger5 = new Passenger("Кирилл");
        Passenger passenger6 = new Passenger("Иван");

        passenger1.goToBus(bus);
        passenger2.goToBus(bus);
        passenger3.goToBus(bus);
        passenger4.goToBus(bus);
        passenger5.goToBus(bus);
        passenger6.goToBus(bus);
        System.out.println(driver.getName() + ": ну и правильно, зачем баласт с собой таскать");

        bus.setDriver(driver);
        System.out.println(driver.getName() + ": Так салаги, наш межгалактический космолет отправляется в путешествие.");
        System.out.println("Смотрите не выпрыгните от страха, поэтому кричите свои имена, чтобы я знал, что вы еще на борту.");
        Thread.sleep(5000);
        driver.drive();
        Driver driver2 = new Driver("МегаВодила", 999999);
        bus.setDriver(driver2);
        Thread.sleep(2000);
        passenger3.leaveBus();
        Thread.sleep(1500);
        System.out.println(bus.isDriving());
        System.out.println("Пассажир " + passenger6.getName() + " хочет попасть на наш " + bus.getModel());
        Thread.sleep(2000);
        passenger6.goToBus(bus);
        System.out.println(bus.isDriving());
        System.out.println("---------------------------");
        for (int i = 0; i < bus.getPassengers().length; i++) {
            if (bus.getPassengers()[i] != null) {
                System.out.println(i + " " + bus.getPassengers()[i].getName());
            } else System.out.println(i + " место свободно");
        }
        Thread.sleep(2000);
        driver.stop();
        System.out.println("Все приехали");
        System.out.println("Водитель " + driver.getName() + " устал,меняем его на " + driver2.getName());
        Thread.sleep(2000);
        bus.setDriver(driver2);
        System.out.println("Слабак - " + passenger3.getName() + " не выдержал нашего путешествия и покидает нас ");
        Thread.sleep(2000);
        passenger3.leaveBus();
        System.out.println("Теперь у нас водитель - " + bus.getDriver().getName());
        Thread.sleep(1500);
        System.out.println("Такс, пересчитаем наших пассажиров:");
        for (int i = 0; i < bus.getPassengers().length; i++) {
            if (bus.getPassengers()[i] != null) {
                System.out.println(i + " " + bus.getPassengers()[i].getName());
            } else System.out.println(i + " место свободно");
        }
    }
}
