
public class Bus {
    private int number;
    private String model;
    private Driver driver;
    private boolean isDriving = false;

    public String getModel() {
        return model;
    }

    public Passenger[] getPassengers() {
        return passengers;
    }

    public Driver getDriver() {
        return driver;
    }

    public boolean isDriving() {
        return isDriving;
    }

    public void setDriving(boolean driving) {
        isDriving = driving;
    }

    public void driving() throws InterruptedException {
        for (Passenger x : passengers) {
            if (!x.equals(null)) {
                System.out.println(x.getName() + " орет во все горло!");
            }
        }
    }

    // массив пассажиров
    private Passenger[] passengers;
    // фактическое количество пассажиров на данный момент
    private int count;

    public Bus(int number, String model, int placesCount) {
        if (number > 0) {
            this.number = number;
        } else {
            this.number = 1;
        }
        this.model = model;
        // создали placesCount объектных переменных в которые можно положить пассажира
        this.passengers = new Passenger[placesCount];
    }

    public void incomePassenger(Passenger passenger) {
        if (!isDriving) {
            // проверяем, не превысили ли мы количество мест?
            if (this.count < this.passengers.length) {
                this.passengers[count] = passenger;
                this.count++;
            } else {
                System.err.println("Автобус переполнен!");
            }
        } else {
            System.out.println("Сорян, останавливаемся только на фиксированных остановках");
        }
    }

    public void removePassenger(Passenger passenger) {
        for (int i = 0; i < passengers.length; i++) {
            if (passengers[i].equals(passenger)) passengers[i] = null;
        }
    }

    public void setDriver(Driver driver) {
        if (!isDriving) {
            this.driver = driver;
            driver.setBus(this);
        } else
            System.err.println("Нельзя заменить " + this.driver.getName() + " на " + driver.getName() + " пока мы находимся в гипперпрыжке!");
    }

    public boolean isFull() {
        return this.count == passengers.length;
    }
}
