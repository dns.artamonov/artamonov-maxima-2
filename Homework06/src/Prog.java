import java.util.Arrays;
import java.util.Scanner;

public class Prog {
    public static void selectionSort(int[] array) {
        int min, indexOfMin;

        for (int i = 0; i < array.length; i++) {
            min = array[i];
            indexOfMin = i;
            for (int j = i; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    indexOfMin = j;
                }
            }
            int temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;
        }
    }

    public static boolean search(int[] array, int element) {
        boolean hasElement = false;
        int left = 0;
        int right = array.length - 1;
        int middle = left + (right - left) / 2;

        while (left <= right) {
            if (element < array[middle]) right = middle - 1;
            else if (element > array[middle]) left = middle + 1;
            else {
                hasElement = true;
                break;
            }
            middle = left + (right - left) / 2;
        }
        return hasElement;
    }

    public static void main(String[] args) {
        int[] array = {10, -4, 90, -91, 56, 23, 9, -200, 123};
        System.out.println("массив до сортировки: " + Arrays.toString(array));
        selectionSort(array);
        System.out.println("массив после сортировки" + Arrays.toString(array));
        System.out.println("Введите число для поиска");
        Scanner scanner = new Scanner(System.in);
        int element = scanner.nextInt();
        System.out.println((search(array, element)) ? "Число найдено" : "Число не найдено");
    }
}
