
public class Main {

    public static void main(String[] args) {

        ArrayList list = new ArrayList();
        list.add(7);
        list.add(10);
        list.add(33);
        list.add(15);
        list.add(14);
        list.add(33);
        list.add(25);
        list.add(33);
        list.add(15);
        list.add(45);
        list.add(99);

        System.out.println("List size=" + list.size());
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        list.removeAt(1);
        System.out.println("removeAt(1):");
        System.out.println("List size=" + list.size());
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        list.remove(15);
        System.out.println("remove(15):");
        System.out.println("List size=" + list.size());
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println("removeLast(33)");
        list.removeLast(33);
        System.out.println("List size=" + list.size());
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println("removeAll(33):");
        list.removeAll(33);
        System.out.println("List size=" + list.size());
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        System.out.println("add (index, element):");
        list.add(3, 145);
        System.out.println("List size=" + list.size());
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println("Linked List:");
        LinkedList linkList = new LinkedList();
        linkList.add(7);
        linkList.add(10);
        linkList.add(33);
        linkList.add(99);
        linkList.add(33);
        linkList.add(20);
        linkList.add(33);
        linkList.add(40);
        linkList.add(-100);
        linkList.add(33);


//        linkList.addToBegin(777);
//        linkList.addToBegin(888);
//        linkList.addToBegin(999);


        for (int i = 0; i < linkList.getSize(); i++) {
            System.out.println(linkList.get(i));
        }
        linkList.add(2,111111);
//                linkList.remove(33);
//        linkList.removeLast(33);
//        linkList.removeAll(33);
//        linkList.reverse();
        System.out.println("----------------");
        for (int i = 0; i < linkList.getSize(); i++) {
            System.out.println(linkList.get(i));
        }


    }
}
