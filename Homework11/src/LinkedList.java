

/**
 * 12.11.2021
 * 15. Lists
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class LinkedList {

    private static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
    }

    // ссылка на первый элемент
    private Node first;
    // ссылка на последний элемент
    private Node last;

    public int getSize() {
        return size;
    }

    private int size = 0;

    public void add(int element) {
        // для нового элемента создаем узел
        Node newNode = new Node(element);
        // если список пустой
        if (isEmpty()) {
            // первый узел = новый узел
            first = newNode;
        } else {
            // следующий после последнего - это новый узел
            last.next = newNode;
        }
        // теперь новый узел - последний
        last = newNode;

        size++;
    }

    public void addToBegin(int element) {
        // для нового элемента создаем узел
        Node newNode = new Node(element);

        if (isEmpty()) {
            last = newNode;
        } else {
            // для нового узла, следующий после него - это первый узел списка
            newNode.next = first;
        }

        // теперь новый узел - первый
        first = newNode;
        size++;
    }

    public int get(int index) {
        // начинаем с первого элемента
        Node current = first;

        int i = 0;

        while (i < size) {
            // если нашли нужный индекс
            if (i == index) {
                // возвращаем значение текущего узла
                return current.value;
            }
            // иначе идем дальше
            current = current.next;
            i++;
        }

        System.err.println("В списке нет такого индекса");
        return -1;
    }

    public void removeAt(int index) {
        Node previousRemove = first;

        if (index == 0) {
            // если удаляется первый элемент, просто сдвигаем ссылку на него
            first = first.next;
            size--;
            return;
        }

        // доходим до индекса элемента, который стоит перед удаляемым
        for (int i = 0; i < index - 1; i++) {
            // переходим по узлам
            previousRemove = previousRemove.next;
        }
        // если удаляем последний
        if (index == (size - 1)) {
            last = previousRemove;
        }
        // в current находится узел, который стоит перед удаляемым
        Node forRemove = previousRemove.next;
        previousRemove.next = forRemove.next;

        size--;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean searchElement(int element) {
        Node curElement = first;
        for (int i = 0; i < size; i++) {
            if (curElement.value == element) {
                return true;
            } else {
                curElement = curElement.next;
            }
        }
        return false;
    }

    public void remove(int element) {
        Node beforeRemove = first;
        int index = 0;
        boolean delete = false;
        if (beforeRemove.value == element) {
            delete = true;
        }
        for (int i = 0; i < size - 1; i++) {
            index++;
            if (beforeRemove.next.value == element) {
                delete = true;
                break;
            } else {
                beforeRemove = beforeRemove.next;
            }
        }
        if (delete) {
            removeAt(index);
        }
    }

    public void removeLast(int element) {
        Node currentRemove = first;
        int index = 0;
        boolean delete = false;
        if (currentRemove.value == element) {
            delete = true;
        }
        for (int i = 0; i < size; i++) {
            if (currentRemove.value == element) {
                index = i;
                delete = true;
            }
            currentRemove = currentRemove.next;
        }
        if (delete) {
            removeAt(index);
        }
    }

    public void removeAll(int element) {
        while (true) {
            if (searchElement(element)) {
                remove(element);
            } else {
                break;
            }
        }
    }

    public void add(int index, int element) {
        Node currentNode = first;
        Node beforeCurrentNode = null;
        Node beforeCurrentNodeNew;

        for (int i = 0; i < size; i++) {
            if ((i == 0 && i == index)) {
                beforeCurrentNodeNew = new Node(element);
                beforeCurrentNodeNew.next = currentNode;
                first = beforeCurrentNodeNew;
                size++;
                break;
            } else if (i == index && i != 0) {
                beforeCurrentNodeNew = new Node(element);
                beforeCurrentNodeNew.next = currentNode;
                beforeCurrentNode.next = beforeCurrentNodeNew;
                size++;
                break;
            } else {
                beforeCurrentNode = currentNode;
                currentNode = currentNode.next;
            }
        }
    }

    public void reverse() {
        Node[] tmparr = new Node[size];
        Node currentNode = first;
        for (int i = 0; i < size; i++) {
            tmparr[i] = currentNode;
            currentNode = currentNode.next;
        }
        first = tmparr[size - 1];
        last = tmparr[0];
        for (int i = size - 1; i > 0; i--) {
            tmparr[i].next = tmparr[i - 1];
        }
    }
}
