public class Prog {
    // функция, для которой считаем определенный интеграл - y = x^2 (парабола), y = sin(x) * 2
    public static double f(double x) {
        return Math.sin(x) * 2;
    }

    // МЕТОД ПРЯМОУГОЛЬНИКОВ
    // функция, которая возвращает значение определенного интеграла для функции f(x) на промежутке от 0 до 10 с разбиением n
    public static double integralByRectangles(double a, double b, int n) {
        // ширина одного прямоугольника
        double width = (b - a) / n;
        // общая сумма площадей всех прямоугольников
        double sum = 0;

        for (double x = a; x <= b; x += width) {
            // считаем значение функции в текущей точке
            double height = f(x);
            // вычисляем площадь текущего прямоугольника
            double currentRectangleArea = height * width;
            // кидаем в общую сумму всех площадей всех прямоугольников
            sum += currentRectangleArea;
        }

        return sum;
    }


    public static double integralBySimpson2(double a, double b, int n) {
        double sum = 0.0;
        double width = (b - a) / n;
        double x = a;
        sum += f(x) * 0.5;
        int i = 2;
        for (; x <= b; x += width) {
            if (i % 2 == 0) sum += 2 * f(x);
            else if (x == b) sum += f(x) * 0.5;
            else sum += f(x);
            i++;
        }
        return sum * 2 * width / 3;
    }

    public static double integralBySimpson(double a, double b, int n) {
        double width = (b - a) / n;
        double sum = 0;
        double k = a + width;
        for (; k + width <= b; k += width * 2) {
            sum += width / 3 * (f(k - width) + 4 * f(k) + f(k + width));

        }
        return sum;
    }

    public static void main(String[] args) {
        // массив с примерами количеств разбиений исходного диапазона
        int[] ns = {10, 100, 1000, 10_000, 50_000, 100_000,
                150_000, 200_000, 300_000, 500_000, 1_000_000, 2_000_000};
        double from = 0;
        double to = 10;
        double realValue = 3.67814305815;

        // массив с результатами работы функции integral для разных разбиений, в ys[i] находится значение для разбиения ns[i]
        double[] ys = new double[ns.length];
        // считаем интегралы для всех разбиений на промежутке от 0 до 10
        for (int i = 0; i < ns.length; i++) {
            ys[i] = integralByRectangles(from, to, ns[i]);
        }
        //вывод результатов
        System.out.println("Метод прямоугольников:");
        for (int i = 0; i < ns.length; i++) {
            System.out.printf("|N = %10d| Y = %10.4f| EPS = %10.5f |\n", ns[i], ys[i], Math.abs(ys[i] - realValue));
        }
        System.out.println("----------------------");
        System.out.println("Метод Симпсона (формула из домашнего задания):");
        for (int i = 0; i < ns.length; i++) {
            ys[i] = integralBySimpson(from, to, ns[i]);
        }
        for (int i = 0; i < ns.length; i++) {
            System.out.printf("|N = %10d| Y = %10.4f| EPS = %10.5f |\n", ns[i], ys[i], Math.abs(ys[i] - realValue));
        }

        System.out.println("----------------------");
        System.out.println("Метод Симпсона2 (формула s=(0,5y0+2y1+y2+2y3+...+2yn-1+0,5yn)"); //сначало по ней решал
        for (int i = 0; i < ns.length; i++) {
            ys[i] = integralBySimpson2(from, to, ns[i]);
        }
        for (int i = 0; i < ns.length; i++) {
            System.out.printf("|N = %10d| Y = %10.4f| EPS = %10.5f |\n", ns[i], ys[i], Math.abs(ys[i] - realValue));
        }
    }

}
