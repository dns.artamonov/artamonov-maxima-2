import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        MapHashImpl<String, Integer> finalMap = new MapHashImpl<>();
        SetHashImpl<String> set = new SetHashImpl<>();

        Scanner sc = new Scanner(System.in);
        String inputString = sc.nextLine();

        //удаляем символы (тестировал на строке: привет, привет! марсель в казани! ты тоже в казани?)
        String[] strings = inputString.split(" ");
        for (int i = 0; i < strings.length; i++) {
            strings[i] = strings[i].replaceAll("([?!:;,])", "");
            set.add(strings[i]);
        }
        System.out.println("Set size = " + set.size());

        int count = 0;

        int index = 0;
        String key;
        while (count < set.size()) {
            int keys = 0;
            key = set.returnKey(index);
            if (key != null) {
                for (int i = 0; i < strings.length; i++) {
                    if (strings[i].equals(key)) {
                        keys++;
                    }
                }
                finalMap.put(key, keys);
                count++;
                index++;
            } else {
                index++;
            }
        }
        System.out.println("Print finalMap:");
        finalMap.print();
    }
}
