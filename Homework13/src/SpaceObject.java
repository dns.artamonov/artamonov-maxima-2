
/**
 * 20.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface SpaceObject {
    void move(double newX, double newY, double newZ);
}
