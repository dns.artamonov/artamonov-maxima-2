public class Logger {
    private final static Logger logger;
    private static double random;

    static {
        logger = new Logger();
        random = Math.random();
    }

    private Logger() {
    }

    public static Logger getLogger() {
        return logger;
    }

    public void log(String message) {
        System.out.println("добавим что нибудь свое (random) " + random + " " + message);
    }

}
