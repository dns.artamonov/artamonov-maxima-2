public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> arr = new ArrayList<>();
        arr.add(10);
        arr.add(1120);
        arr.add(-123410);
        for (int i = 0; i < arr.size(); i++) {
            System.out.println(arr.get(i));
        }
        System.out.println("-----------");
        ArrayList<String> arr1 = new ArrayList<>();
        arr1.add("Как дел?");
        arr1.add("Отдыхаю");
        arr1.add("Наконец то");
        for (int i = 0; i < arr1.size(); i++) {
            System.out.println(arr1.get(i));
        }
        System.out.println("-----------------------");
        ArrayList<TestObject> arr2 = new ArrayList<>();
        arr2.add(new TestObject("Денис"));
        arr2.add(new TestObject("Олег"));
        arr2.add(new TestObject("Дмитрий"));
        for (int i = 0; i < arr2.size(); i++) {
            System.out.println(arr2.get(i)+" "+arr2.get(i).getName());
        }
        System.out.println("removeAt");
        arr2.removeAt(1);
        for (int i = 0; i < arr2.size(); i++) {
            System.out.println(arr2.get(i)+" "+arr2.get(i).getName());
        }

    }
}
