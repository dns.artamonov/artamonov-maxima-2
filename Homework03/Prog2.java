import java.util.Scanner;

public class Prog2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int first = sc.nextInt();
        int second = sc.nextInt();
        int third = sc.nextInt();
        
        while (third != -1) {
            if (first > second && second < third) {
                System.out.println("Local minimum is " + second);
                first = second;
                second = third;
                third = sc.nextInt();
            } else {
                first = second;
                second = third;
                third = sc.nextInt();
            }
        }
    }
}
