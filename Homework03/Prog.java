import java.util.Scanner;

public class Prog {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int curInt = sc.nextInt();
        int min = Integer.MAX_VALUE;
        while (curInt != -1) {
            int digit;
            int curInt2 = curInt;
            int sum = 0;
            while (curInt2 != 0) {
                digit = curInt2 % 10;
                curInt2 /= 10;
                sum = sum + digit;
            }
            if (sum < min) min = curInt;
            curInt = sc.nextInt();
        }
        System.out.println("Minimal sum digits in = " + min);
    }
}
