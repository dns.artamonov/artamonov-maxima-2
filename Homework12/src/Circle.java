public class Circle extends Ellipse {
    public Circle(int x, int y, double a) {
        super(x, y, a, a);
    }

    public double getPerimeter() {
        return 2 * Math.PI * a;
    }
}
