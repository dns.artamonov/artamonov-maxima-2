public class Ellipse extends Figure {
    protected double a;
    protected double b;

    public Ellipse(int x, int y, double a, double b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    public double getPerimeter() {
        return 4 * (Math.PI * a * b + Math.pow((a - b), 2)) / (a + b);
    }

}
