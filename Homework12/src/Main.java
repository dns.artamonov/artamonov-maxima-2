public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(0, 0, 10, 15);
        Square square = new Square(0, 0, 5);
        Ellipse ellipse = new Ellipse(0, 0, 5.0, 2.0);
        Circle circle = new Circle(0, 0, 5);
        System.out.println(ellipse.getPerimeter());
        System.out.println(circle.getPerimeter());
        System.out.println(rectangle.getPerimeter());
        System.out.println(square.getPerimeter());
        rectangle.move(66, 77);
        square.move(66, 77);
        ellipse.move(66, 77);
        circle.move(66, 77);
        System.out.println(rectangle.getXY());
        System.out.println(square.getXY());
        System.out.println(ellipse.getXY());
        System.out.println(circle.getXY());
    }
}
